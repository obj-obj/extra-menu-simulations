-- Get the menu simulations
local menuSims = data.raw["utility-constants"]["default"].main_menu_simulations

-- Fix some vanilla menu simulations
menuSims.brutal_defeat.save = "__Extra_Menu_Simulations__/saves/brutal-defeat-fix.zip"
menuSims.burner_city.save = "__Extra_Menu_Simulations__/saves/burner-city-fix.zip"
menuSims.early_smelting.save = "__Extra_Menu_Simulations__/saves/early-smelting-fix.zip"
menuSims.mining_defense.save = "__Extra_Menu_Simulations__/saves/mining-defense-fix.zip"
menuSims.lab.save = "__Extra_Menu_Simulations__/saves/lab-fix.zip"

-- Remove vanilla menu simulations that don't work
menuSims.spider_ponds = nil
menuSims.biter_base_spidertron = nil

-- Add new simulations
function createMenuSim(name, length, update, init)
    init = init or [[]]
    update = update or [[]]

    menuSims[name] = {
        checkboard = false,
        save = "__Extra_Menu_Simulations__/saves/" .. name .. ".zip",
        length = 60 * length,
        init = [[
        local logo = game.surfaces.nauvis.find_entities_filtered{name = "factorio-logo-11tiles", limit = 1}[1]
        game.camera_position = {
            logo.position.x,
            logo.position.y + 9.75
        }
        game.camera_zoom = 1
        game.tick_paused = false
        game.surfaces.nauvis.daytime = 0
        ]] ..
            init,
        update = update
    }
end

-- Change some simulation times
menuSims.solar_power_construction.length = 60 * 10
menuSims.brutal_defeat.length = 60 * 15
menuSims.artillery.length = 60 * 20

-- Process settings
for name, simulation in pairs(menuSims) do
    menuSims[name].length = menuSims[name].length * settings.startup["obj_menusim_duration-multiplier"].value
end
