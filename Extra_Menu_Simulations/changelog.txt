---------------------------------------------------------------------------------------------------
Version: 1.1.1
Date: 26.3.12021
  Fixes:
    - Hopefully make the mod not die when installing
---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 23.3.12021
  Features:
    - Removed the two custom simulations, as they were pretty bad compared to the vanilla ones. Better ones coming soon™!
  Fixes:
    - Fixed 2 of the remaining 4 vanilla simulations that don't work (the 2 left are the Spidertron simulations), redid the Burner City and Early Smelting fixes
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 21.1.12021
  Features:
    - Added another menu simulation (a small logistic train network with 3 stations)
  Fixes:
    - Removed the random player characters in the added menu simulations
    - Changed the timings of 2 simulations slightly
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 20.1.12021
  Features:
    - Added a menu simulation
  Fixes:
    - Removed two more vanilla menu simulations that didn't work (might fix them soon)
---------------------------------------------------------------------------------------------------
Version: 0.1.1
Date: 16.1.12021
  Features:
    - Compressed all 20 settings into one setting
  Fixes:
    - Added locale for English
---------------------------------------------------------------------------------------------------
Version: 0.1.0
Date: 15.1.12021
  Features:
    - Fixed and/or removed the vanilla backgrounds that don't work with Krastorio 2 / Space Exploration / AAI Industry
    - Added settings to change the length of the menu simulations