data:extend(
    {
        {
            type = "int-setting",
            name = "obj_menusim_duration-multiplier",
            setting_type = "startup",
            default_value = 1,
            minimum_value = 0.1,
            maximum_value = 100
        }
    }
)
